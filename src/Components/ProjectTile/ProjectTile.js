import React, {Component} from 'react';
import PropTypes from 'prop-types';
import './style.css'
import classNames from 'classnames';
// import image
import foto from './coaching.jpg';

export default class ProjectTile extends Component {

  static defaultProps = {
		size: 'single',
	};

	static propTypes = {
		size: PropTypes.oneOf([
			'single',
			'double',
			'triple',
		]),
		onClick: PropTypes.func,
	};

  render() {
    const { size } = this.props


    return (
      <div 
        className={classNames('ProjectTile', size )}
        style={{backgroundImage: `url(${foto})`}} 
      >
        <div className={classNames('overlay', size)}>
          <p>Project name</p>
        </div>
      </div>
    )
  }
}