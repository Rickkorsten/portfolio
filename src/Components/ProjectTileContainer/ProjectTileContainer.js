import React, {Component} from 'react';
import { ProjectTile, AddProjectTile } from '../'
import './style.css'

export default class ProjectTileContainer extends Component {
  render() {
    return (
      <div className="ProjectTileContainer">
      
            <ProjectTile size='triple'></ProjectTile>
            <ProjectTile size='single'></ProjectTile>
            <ProjectTile size='double'></ProjectTile>
            <ProjectTile size='single'></ProjectTile>
            <ProjectTile size='single'></ProjectTile>
            <ProjectTile size='single'></ProjectTile>
            <ProjectTile size='triple'></ProjectTile>
            <AddProjectTile />
      </div>
    )
  }
}