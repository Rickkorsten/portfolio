import React, {Component} from 'react';
import {Link} from 'react-router-dom'
import './style.css';

export default class Navigation extends Component {
  render() {
    return (

<div className="App-navigation">
              <div><Link to="/">Home</Link></div>
              <div><Link to="/projects">Projects</Link></div>
              <div><Link to="/contact">Contact</Link></div>
            </div>
    )
}
}