import React, {Component} from 'react';
import {BrowserRouter as Router, Route, Link, Switch} from 'react-router-dom'
import './App.css';
import {Navigation} from './Components';

import Home from './Views/Home/Home';
import Projects from './Views/Projects/Projects';

export default class App extends Component {
  render() {
    return (
      <div className="App">
          <Router>
          <div>
          <Navigation />
          <div className='container'>
          <Switch>
            <Route exact path="/" component={Home}/>
            <Route path="/projects" component={Projects}/>
            </Switch>
            </div>
          </div>
        </Router>
      </div>
    )
  }
}